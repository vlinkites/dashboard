// Constructor
function settings (environment) {
	// Public
	this.environment = "dev"
}

//public member variable
settings.prototype.siteurl = "http://dash.sirvi.com";
settings.prototype.localsiteurl = "http://localhost/healthypeps";
settings.prototype.apiurl = "http://104.131.124.227:3000/api";
 
// Public
settings.prototype.getsiteurl = function () {
	if(this.environment=="local"){
		return this.localsiteurl;
	}
	return this.siteurl;
};

// Public
settings.prototype.getapiurl = function () {
	return this.apiurl;
};  