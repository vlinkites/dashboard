<?php
@session_start();
include 'Services/Twilio/Capability.php';
$username = '';
$imageurl = 'images/noimage-small.png';
if (!isset($_SESSION["userid"])) {
    header("location:index.php");
} else {
    $username = $_SESSION["username"];
    if (isset($_SESSION["imageurl"]) && $_SESSION["imageurl"] != null) {
        $imageurl = $_SESSION["imageurl"];
    }
}
// put your Twilio API credentials here
$accountSid = 'AC50bcbef90dd37cf6f5cb5f8ef13964c9';
$authToken = '49d0225d70d46d9e57324e98c48d3b22';
$sqootApiKey = 'f07pod';
// put your Twilio Application Sid here
$appSid = 'APa3f3e49d38f8f607c580e1d82ac82b96';
$capability = new Services_Twilio_Capability($accountSid, $authToken);
$capability->allowClientOutgoing($appSid);
$capability->allowClientIncoming('support');
$token = $capability->generateToken();
?>
<!doctype html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
        <title>Dashboard</title>
        <link href="css/bootstrap.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
        <script type="text/javascript" src="config/settings.js"></script>
    </head>
    <body>
        <div id="divmainstart">
            <?php 
                $_SESSION["startup"]="true";
                include('start.php'); 
            ?>
        </div>
        <div id="divmaindashboard" style="display:none;">
           <?php include('dashboard_partial.php'); ?>
        </div>
        <script type="text/javascript" src="//static.twilio.com/libs/twiliojs/1.2/twilio.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="js/dashboard.js"></script> 
        <script>
            var tc = '<?php echo trim($token); ?>';
            var k = '<?php echo $sqootApiKey;?>';
            var agentname = '<?php echo $username;?>';
        </script>
        <script type="text/javascript" src="js/twilio.client.dashboard.js"></script> 
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.zclip.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script src="js/jquery.mCustomScrollbar.js"></script>
        <script>
            $(document).ready(function () {
                $(".checkboxwrap").mCustomScrollbar();
                populateCategories(k,agentname);
                populateIntrests();
                populateQuestions();
                $(".dropdown-menu li a").click(function(e){
                  $(".btn:first-child").text($(this).text());
                  $(".btn:first-child").val($(this).text());
                  transfercall(e,$(this).attr("num"));
                });                 
                $(".btn-forward").hide();
                getCallerInProgress();
            });
        </script>
    </body>
</html>