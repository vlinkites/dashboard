<div class="wrapper">
<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9 nopaddingrt header-left">
                <div class="brow">
                    <div class="brow_ard pull-left">
                        <div class="pull-left">
                            <i class="fa  fa-map-marker pull-left"></i>
                            <div class="pull-right valign">
                                <span class="country city">BROWARD</span>
                                <p class="state">FLORIDA 33311</p>
                            </div>
                        </div>
                        <div class="pull-right">
                            <div class="pull-left temp_day forecast" onclick="showForeCast()">
                                <span class="temp"><label id="temp">89</label>&deg;</span>
                                <p class="day">SUNDAY</p>
                            </div>
                            <i class="fa fa-caret-left pull-right"></i>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="rating pull-right">
                        <div class="pull-left left_border intent-wrapper">
                            <h1 style="color:#fff;">CALL INTENT:</h1>
                        </div>
                        <div class="intent-holder"></div>
                        <div class="pull-right call-box">
                            <h2 class="call">CALL TIME <span id="timer">00:00</span></h2>
                            <div class="btn-hangup"><span onclick="hangup();">Hang Up Call</span></div>
                            <div class="btn-group btn-forward">
                                    <i class="fa fa-phone icon-forward"></i>
                                    <button type="button" data-toggle="dropdown" class="btn btn-forward-list dropdown-toggle">Forward<span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" num="19546103618">Omar (+19546103618)</a></li>
				<li><a href="#" num="919179839586">Jitendra (+919179839586)</a></li>
				<li><a href="#" num="918103342731">Virendra (+918103342731)</a></li>
                                    <li class="divider"></li>
                                </ul>
                        </div>
                        </div>
                             
                        </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="customer">
                    <div class="line"></div>
                    <div class="pull-left">
                        <h1><?php echo $username; ?></h1>
                        <p>CUSTOMER SERVICE REP</p>
                        <div class="checkbox check-primary">
                            <input id="LOG" type="checkbox" value="1">
                            <label for="LOG"><a href="logout.php" style="color:#fff;">LOG ME OUT</a></label>
                        </div>
                    </div>
                    <div class="profile pull-right">
                        <img src="<?php echo $imageurl; ?>" alt="" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="main_box">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="caller">
                                <div class="caller_img">
                                    <img id="callerimage" src="images/noimage.png" />
                                    <div class="remaining_calls">
                                        <span class="nunber pull-left">2</span> <span class="remainings pull-right">CALLS<br />
                                            REMAINING</span>
                                    </div>
                                </div>
                                <h2 id="callername">Ed Leon</h2>
                                <h3 id="callernumber">(502) 553-97 05</h3>
                                <h1 id="callerlevel">MEMBERSHIP LEVEL</h1>
                                <div class="caller_device">
                                    <div class="apple"><i class="fa fa-apple"></i></div>
                                    <span class="left">CALLER</span><span class="right">DEVICE</span>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="accordions">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-caret-right"></i>BASIC INFORMATION </a></h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="col-xs-6">
                                                    <label for="">Address:</label>
                                                    <input type="text" id="inputAddress" name="" value="" placeholder="" />
                                                    <br />
                                                    <label for="">Phone:</label>
                                                    <input type="text" id="inputPhone" name="" value="" placeholder="" />
                                                    <br />
                                                    <label for="">City:</label>
                                                    <input type="text" id="inputCity" name="" value="" placeholder="" />
                                                    <br />
                                                    <label for="">State:</label>
                                                    <input class="state" type="text" id="inputState" name="" value="" placeholder="" />
                                                    <label class="text-center" for="">Zip:</label>
                                                    <input class="zip" type="text" id="inputZip" name="" value="" placeholder="" />
                                                    <br />
                                                    <label for="">Email:</label>
                                                    <input type="text" id="inputEmail" name="" value="" placeholder="" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <label for="">Occupation:</label>
                                                    <input type="text" id="inputOccupation" name="" value="" placeholder="" />
                                                    <br />
                                                    <label for="">Company:</label>
                                                    <input type="text" id="inputCompany" name="" value="" placeholder="" />
                                                    <br />
                                                    <label for="">Address:</label>
                                                    <input type="text" id="inputCompanyAddress" name="" value="" placeholder="" />
                                                    <br />
                                                    <label for="">Phone:</label>
                                                    <input type="text" id="inputCompanyPhone" name="" value="" placeholder="" />
                                                    <br />
                                                    <label for="">Email:</label>
                                                    <input type="text" id="inputCompanyEmail" name="" value="" placeholder="" />
                                                </div>
                                                <div class="col-xs-12">
                                                    <label for="">Education: </label>
                                                    <select id="education">
                                                        <option>GED</option>
                                                        <option>Assoc.</option>
                                                        <option>Degree</option>
                                                        <option>Bachelors </option>
                                                        <option>Masters</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><i class="fa fa-caret-right"></i>WORK </a></h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><i class="fa fa-caret-right"></i>EDUCATION </a></h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><i class="fa fa-caret-right"></i>SOCIAL LINKS </a></h4>
                                        </div>
                                        <div id="collapseSix" class="panel-collapse collapse">
                                            <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading heading-family">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsefamily"><i class="fa fa-caret-right"></i>FAMILY </a></h4>
                                        </div>
                                        <div id="collapsefamily" class="panel-collapse collapse">
                                            <div class="panel-body family"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading heading-health">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsehealth"><i class="fa fa-caret-right"></i>HEALTH </a></h4>
                                        </div>
                                        <div id="collapsehealth" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading heading-personalinjury">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsepersonalinjury"><i class="fa fa-caret-right"></i>PERSONAL INJURY</a></h4>
                                        </div>
                                        <div id="collapsepersonalinjury" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading heading-bankruptcy">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsebankruptcy"><i class="fa fa-caret-right"></i>BANKRUPTCY</a></h4>
                                        </div>
                                        <div id="collapsebankruptcy" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading heading-dui">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsedui"><i class="fa fa-caret-right"></i>DUI</a></h4>
                                        </div>
                                        <div id="collapsedui" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading heading-tax">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsetax"><i class="fa fa-caret-right"></i>TAX</a></h4>
                                        </div>
                                        <div id="collapsetax" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><i class="fa fa-caret-right"></i>Queued Calls</a></h4>
                                        </div>
                                        <div id="collapseSeven" class="panel-collapse collapse">
                                            <div class="panel-body"></div>
                                        </div>
                                    </div>
				                    <div class="panel panel-default inprogress-container">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><i class="fa fa-caret-right"></i>In-Progress Calls</a></h4>
                                        </div>
                                        <div id="collapseEight" class="panel-collapse collapse">
                                            <div class="panel-body"></div>
                                        </div>
                                    </div>
                                    -->                                                
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="accordions">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading heading-legaldocuments">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapselegaldocuments"><i class="fa fa-caret-right"></i>LEGAL DOCUMENTS </a></h4>
                                        </div>
                                        <div id="collapselegaldocuments" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading heading-supplemental">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsesupplemental"><i class="fa fa-caret-right"></i>SUPPLEMENTAL</a></h4>
                                        </div>
                                        <div id="collapsesupplemental" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading heading-life">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapselife"><i class="fa fa-caret-right"></i>LIFE</a></h4>
                                        </div>
                                        <div id="collapselife" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading heading-disability">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsedisability"><i class="fa fa-caret-right"></i>DISABILITY</a></h4>
                                        </div>
                                        <div id="collapsedisability" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading heading-longtermcare">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapselongtermcare"><i class="fa fa-caret-right"></i>LONG TERM CARE</a></h4>
                                        </div>
                                        <div id="collapselongtermcare" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading heading-entertainment">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseentertainment"><i class="fa fa-caret-right"></i>ENTERTAINMENT</a></h4>
                                        </div>
                                        <div id="collapseentertainment" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading heading-dining">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsedining"><i class="fa fa-caret-right"></i>DINING</a></h4>
                                        </div>
                                        <div id="collapsedining" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading heading-travel">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsetravel"><i class="fa fa-caret-right"></i>TRAVEL</a></h4>
                                        </div>
                                        <div id="collapsetravel" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading heading-homeauto">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsehomeauto"><i class="fa fa-caret-right"></i>HOME/AUTO</a></h4>
                                        </div>
                                        <div id="collapsehomeauto" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>                                                
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading heading-sirvipays">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapsesirvipays"><i class="fa fa-caret-right"></i>SIRVI PAYS</a></h4>
                                        </div>
                                        <div id="collapsesirvipays" class="panel-collapse collapse">
                                            <div class="panel-body"><div class="col-xs-12"></div></div>
                                        </div>
                                    </div>                                               
                                    <!--
                                    <div class="panel panel-default queue-container">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><i class="fa fa-caret-right"></i>Queued Calls</a></h4>
                                        </div>
                                        <div id="collapseSeven" class="panel-collapse collapse">
                                            <div class="panel-body"></div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default inprogress-container">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><i class="fa fa-caret-right"></i>In-Progress Calls</a></h4>
                                        </div>
                                        <div id="collapseEight" class="panel-collapse collapse">
                                            <div class="panel-body"></div>
                                        </div>
                                    </div>
                                    -->                                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="interests">
                        <div class="interest_head clearfix"><i class="fa fa-map-marker"></i><span>INTERESTS</span></div>
                        <input id="searchInterest" type="search" name="" value="" placeholder="Search"/>
                        <div class="row checkboxwrap interestwrap-2">
                            <div class="vspace-5"></div>
                            <div class="col-xs-12 col-sm-12 col-md-6 pull-left interest-left"></div>
                            <div class="col-xs-12 col-sm-12 col-md-6 pull-right interest-right"></div>
                        </div>
                    </div>
                </div>
                <div class="vspace-10"></div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
						<div class="col-xs-12 col-sm-12 col-md-9">
							<div class="map-wrapper">
								<div class="categories">
									<div class="categories-data">
										<div class="interest_head clearfix"><i class="fa fa-bars"></i><span>CATEGORIES</span></div>
										<input id="searchCategory" type="search" name="" value="" placeholder="Search" />
										<div class="row checkboxwrap checkboxwrap-2">
                                            <div class="vspace-5"></div>
											<div class="col-xs-12 col-sm-12 col-md-6 pull-left category-left"></div>
											<div class="col-xs-12 col-sm-12 col-md-6  pull-right category-right"></div>
										</div>
										<div class="clear"></div>												
										<div class="vspace-10"></div>
										<div class="router" style="display:none;">
											<div class="router_box">
												<img src="images/road.png" alt="">
												ROUTE</div>
											<div class="pull-left left_part">
												<label for="">TIME: </label>
												<input type="text" name="" value="" />
												<label for="">DISTANCE: </label>
												<input type="text" name="" value="" />
											</div>
											<div class="pull-right right_part">
												<label for="">TRAFFIC: </label>
												<input type="text" name="" value="" />
												<label for="">HOW: </label>
												<input type="text" name="" value="" />
											</div>
										</div>
									</div>
								</div>

								<style type="text/css">
									#map-canvas { 
										height: 500px;
										width: 100%;
									}
                                    #map-canvas img { max-width: inherit; }
									.controls {
										margin-top: 16px;
										border: 1px solid transparent;
										border-radius: 2px 0 0 2px;
										box-sizing: border-box;
										-moz-box-sizing: border-box;
										height: 32px;
										outline: none;
										box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
									}
									#pac-input {
										background-color: #fff;
										padding: 0 11px 0 13px;
										width: 400px;
										font-family: Roboto;
										font-size: 15px;
										font-weight: 300;
										text-overflow: ellipsis;
										float:right;
									}

									#pac-input:focus {
										border-color: #4d90fe;
										margin-left: -1px;
										padding-left: 14px;  /* Regular padding-left + 1. */
										width: 401px;
									}

									.pac-container {
										font-family: Roboto;
									}

									#type-selector {
										color: #fff;
										background-color: #4d90fe;
										padding: 5px 11px 0px 11px;
									}

									#type-selector label {
										font-family: Roboto;
										font-size: 13px;
										font-weight: 300;
									}
                                    .gm-style-iw {
                                                    width: 500px; 
                                        height: auto;
                                        overflow: hidden;
                                    }
                                    .sendbutton{
                                                    background-color: #3edf3b; border: medium none; text-decoration: none; color: #ffffff; font-size: 20px; text-transform: uppercase; font-weight: 400;padding: 0 20px; border: 1px solid #3edf3b; border-radius: 8px;margin-top: -20px;
                                    }
                                                .sendbutton:hover, .sendbutton:focus{
                                                    background-color: #3edf3b; text-decoration: none; color: #ffffff;
                                    }

                                                .gm-style-iw > div, .gm-style-iw > div > div { overflow: inherit !important; }
                                                .deal-title { margin-bottom: 5px; font-size: 16px; font-weight: 400; line-height: 1.2827; }
                                                .deal-category, .deal-address { color: #a0a0a0; margin-bottom: 5px; font-weight: 400; }
                                                .deal-subtitle { color: #838383; font-weight: 400; margin-bottom: 5px; }
                                                .deal-photo { width: 80px; height: auto; }
								</style>
								<input id="pac-input" class="controls" type="text" placeholder="Search Box">
								<div id="map-canvas"></div>
							</div>
						</div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="deals clearfix">
                                <i class="">
                                    <img src="images/labelimg.png" alt=""></i> <span>DAILY DEALS</span>
                                <div class="featured">
                                    <div class="line"></div>
                                    <h2>Featured Today</h2>
                                    <p>SEPTEMBER 15, 2014</p>
                                </div>
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse11"><i class="pull-left">
                                                        <img src="images/cart.png" alt=""></i>
                                                    <div class="shop pull-left">
                                                        <h1>SHOP</h1>
                                                        <p>DESCRIPTION OF DEAL</p>
                                                    </div>
                                                    <i class="fa fa-caret-left pull-right"></i>
                                                    <div class="clear"></div>
                                                </a></h4>
                                        </div>
                                        <div id="collapse11" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Text hear</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse12"><i class="pull-left">
                                                        <img src="images/diner.png" alt=""></i>
                                                    <div class="shop pull-left">
                                                        <h1>FOOD</h1>
                                                        <p>DESCRIPTION OF DEAL</p>
                                                    </div>
                                                    <i class="fa fa-caret-left pull-right"></i>
                                                    <div class="clear"></div>
                                                </a></h4>
                                        </div>
                                        <div id="collapse12" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Text hear</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse13"><i class="pull-left">
                                                        <img src="images/cocktail.png" alt=""></i>
                                                    <div class="shop pull-left">
                                                        <h1>DRINKS</h1>
                                                        <p>DESCRIPTION OF DEAL</p>
                                                    </div>
                                                    <i class="fa fa-caret-left pull-right"></i>
                                                    <div class="clear"></div>
                                                </a></h4>
                                        </div>
                                        <div id="collapse13" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Text hear</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vspace-10"></div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-9">
                        <div class="">
                            <div class="ratings">
                                <div class="col-xs-6 nopaddingrt dealodd">                                       

                                </div>
                                <div class="col-xs-6 padddingleft5 dealeven">

                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <div class="tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs">
                                        <li class="first_tab active"><a href="#call" data-toggle="tab"><i class="fa fa-map-marker"></i><span>CALL HISTORY</span></a></li>
                                        <li class="secound_tab"><a href="#recom" data-toggle="tab"><i class="fa fa-map-marker"></i><span>RECOMMENDATIONS</span></a></li>
                                        <li class="third_tab"><a href="#notes" data-toggle="tab"><i class="fa fa-map-marker"></i><span>NOTES</span></a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active checkboxwrap tabwrap" id="call">
                                            <ul>
                                                <li><span class="one">THURS</span> <span class="two">SEPTEMBER 15, 2014</span> <span class="three">12:32 AM</span> <span class="four">MOVIE TICKETS</span> </li>
                                                <li><span class="one">TUES</span> <span class="two">AUGUST 15, 2014</span> <span class="three">4:56 PM</span> <span class="four">DAY CARE</span> </li>
                                                <li><span class="one">SUN</span> <span class="two">JULY 15, 2014</span> <span class="three">2:45 PM</span> <span class="four">SALON</span> </li>
                                                <li><span class="one">THURS</span> <span class="two">SEPTEMBER 15, 2014</span> <span class="three">12:32 AM</span> <span class="four">MOVIE TICKETS</span> </li>
                                                <li><span class="one">TUES</span> <span class="two">AUGUST 15, 2014</span> <span class="three">4:56 PM</span> <span class="four">DAY CARE</span> </li>
                                                <li><span class="one">SUN</span> <span class="two">JULY 15, 2014</span> <span class="three">2:45 PM</span> <span class="four">SALON</span> </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane checkboxwrap tabwrap" id="recom">
                                            <ul>
                                                <li><span class="one2">MOVIE THEATER</span> <span class="two2">REGAL OAKWOOD CINEMA - IRON MAN MOVIE</span> <span class="three2"><i class="fa fa-heart-o"></i></span></li>
                                                <li><span class="one2">DAYCARE HOURS</span> <span class="two2">MON - FRI 7 AM - 9 PM, SAT & SUN 8 AM - 8 PM</span> <span class="three2"><i class="fa fa-heart-o"></i></span></li>
                                                <li><span class="one2">SALON APMT</span> <span class="two2">PARADISE NAIL SALON - 9 AM WITH NANCY</span> <span class="three2"></span></li>
                                                <li><span class="one2">MOVIE THEATER</span> <span class="two2">REGAL OAKWOOD CINEMA - IRON MAN MOVIE</span> <span class="three2"><i class="fa fa-heart-o"></i></span></li>
                                                <li><span class="one2">DAYCARE HOURS</span> <span class="two2">MON - FRI 7 AM - 9 PM, SAT & SUN 8 AM - 8 PM</span> <span class="three2"><i class="fa fa-heart-o"></i></span></li>
                                                <li><span class="one2">SALON APMT</span> <span class="two2">PARADISE NAIL SALON - 9 AM WITH NANCY</span> <span class="three2"></span></li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane checkboxwrap tabwrap" style="overflow:auto;max-height:300px !important;" id="notes">
                                            <div id="notesinner">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <div class="message">
                            <i class="fa fa-pencil"></i><span>SEND MESSAGE</span>
                            <div class="clear"></div>
                            <div class="sms">
                                <i>
                                <img src="images/message1.png" alt=""></i>
                                <input type="text" name="" value="SMS" onfocus="if (this.value == 'SMS')
                                            this.value = '';" onblur="if (this.value == '')
                                                        this.value = 'SMS'" />
                            </div>
                            <div class="push">
                                <i>
                                    <img src="images/message1.png" alt=""></i>
                                <input type="text" name="" value="PUSH" onfocus="if (this.value == 'PUSH')
                                            this.value = '';" onblur="if (this.value == '')
                                                        this.value = 'PUSH'" />
                            </div>
                            <div class="email">
                                <i>
                                    <img src="images/message2.png" alt=""></i>
                                <input type="email" name="" value="EMAIL" onfocus="if (this.value == 'EMAIL')
                                            this.value = '';" onblur="if (this.value == '')
                                                        this.value = 'EMAIL'" />
                            </div>
                            <div class="textarea">
                                <textarea id="txtMessage" name="" cols="40" rows="6"></textarea>
                                <input type="submit" value="SEND" name="" />
                                <input type="reset" value="CANCEL" name="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer></footer>
</div>
<div id="myModal" class="modal fade subscribe" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <center>
        <div class="mobile_screen">
            <div class="img_screen">
                <img src="images/mobile_top_bar.png" />
                <div class="shadow">
                    <img src="images/mobile_top_shadow.png" />
                    <div class="text">
                        <h1><span id="callername"></span></h1>
                        <p><span id="callerphone"></span></p>
                    </div>
                </div>
                <div class="screen">
                    <img id="callerbgimg" src="images/mobile_screen2.png" /></div>
            </div>
            <div class="btn"><i class="fa fa-phone"></i><span onclick="call();">Answer Call</span></div>
        </div>
    </center>
</div>
<div class="modal fade" id="notesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog notes-pop-up">
        <div class="modal-content"> 
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title notes-title" id="myModalLabel">Call notes</h4>
                <form method="post">
                    <div class="form-group">
                     <label for="" class="control-label notes-label">Intent</label>
                     <select id="agentnotesintent" class="form-control notes-content">
                     <option>FAMILY</option>
                     <option>HEALTH</option>
                     <option>PERSONAL INJURY</option>
                     <option>BANKRUPTCY</option>
                     <option>DUI</option>
                     <option>TAX</option>
                     <option>LEGAL DOCUMENTS</option>
                     <option>SUPPLEMENTAL</option>
                     <option>LIFE</option>
                     <option>DISABILITY</option>
                     <option>LONG TERM CARE</option>
                     <option>ENTERTAINMENT</option>
                     <option>DINING</option>
                     <option>TRAVEL</option>
                     <option>HOME/AUTO</option>
                     </select>
                    </div>
                    <div class="form-group">
                         <label for="" class="control-label notes-label">Resolution</label>
                         <select id="agentnotesresolution" name="agentnotesresolution"  class="form-control notes-content">
                         </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label notes-label">Additional Notes</label>
                        <textarea id="agentnotes" onkeyup="clearerror();" class="form-control" rows="3"></textarea>
                    </div>  
                    <button type="button" id="btnNotesSave" class="btn btn-large save-btnn" onclick="saveAgentNotes('<?php echo $_SESSION["userid"]; ?>','<?php echo $_SESSION["username"]; ?>');">Save Notes</button>
                    <br/>
                    <span id="spanNotesSaved"></span>
                    <button type="button" id="btnCloseNotes" class="btn btn-large save-btnn" style="display:none;" onclick="closenotes();">Close</button>
                    <!--<button type="button" class="btn btn-large cancel-btnn">Cancel</button>-->
                </form>
            </div>
        </div>
     </div>
    </div>
<div id="forecastModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-elem">
            <div class="clearfix"> 
                <h4 class="modal-title">Forecast <span class="fa fa-times-circle cls-popup" data-dismiss="modal" data-target="#forecastModal"></span></h4>
            </div>
            <div class="table-responsive">
                <table class="table table-stripped table-bordered table-hover tbl-data" id="tblforecast" border="0" cellpadding="5" cellspacing="5" width="100%">
                    <thead>
                        <tr>
                          <th>Date</th>
                          <th>High</th>
                          <th>Low</th>
                          <th>Notes</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>  
    </div>
</div>
<input type="hidden" id="txtSlugs" value=""/>            