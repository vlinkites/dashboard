var config = new settings();
var map;
var seconds = 0;
var gMarkers = [];
var categories = [];
var interests = [];
var questions = [];
var slugs = [];
var types = ["accounting","airport","amusement_park","aquarium","art_gallery","atm","bakery","bank","bar","beauty_salon","bicycle_store","book_store","bowling_alley","bus_station","cafe","campground","car_dealer","car_rental","car_repair","car_wash","casino","cemetery","church","city_hall","clothing_store","convenience_store","courthouse","dentist","department_store","doctor","electrician","electronics_store","embassy","establishment","finance","fire_station","florist","food","funeral_home","furniture_store","gas_station","general_contractor","grocery_or_supermarket","gym","hair_care","hardware_store","health","hindu_temple","home_goods_store","hospital","insurance_agency","jewelry_store","laundry","lawyer","library","liquor_store","local_government_office","locksmith","lodging","meal_delivery","meal_takeaway","mosque","movie_rental","movie_theater","moving_company","museum","night_club","painter","park","parking","pet_store","pharmacy","physiotherapist","place_of_worship","plumber","police","post_office","real_estate_agency","restaurant","roofing_contractor","rv_park","school","shoe_store","shopping_mall","spa","stadium","storage","store","subway_station","synagogue","taxi_stand","train_station","travel_agency","university","veterinary_care","zoo"];
var currentinfo = null;
var showForecast = false;
var appUserId = '';
var appUserToken = ''; 
var agentname = '';
var userDeviceToken = ''
var userDeviceType = '';
function includeRequiredJs(jsname,pos) {
	var th = document.getElementsByTagName(pos)[0];
	var s = document.createElement('script');
	s.setAttribute('type','text/javascript');
	s.setAttribute('src',jsname);
	th.appendChild(s);
} 


function popData(uid, at, lat, lng, key) {
	uid = uid.replace("client:", "");                        
	getNotes(uid);	
	// Get callers personal/professional/family information based on userid send by the srivi app
	$.ajax({
	url: config.getapiurl()+"/calls/findOne?filter=%7B%22where%22%3A%7B%22appUserId%22%3A%22"+uid+"%22%7D%7D",
	type: 'GET',
	data: {},
	success: function (data) {
		if(data.appUserId == uid)
		{
			at = data.access_token;
			appUserToken = at;
			appUserId = data.appUserId;
			$.ajax({
				url: config.getapiurl()+"/appUsers/" + appUserId + "?access_token=" + at,
				type: 'GET',
				data: {},
				success: function (result) {
					if (result.id)
					{
						lat = result.last_location.lat;
						lng = result.last_location.lng;
						//Initialize map
						initialize(lat,lng);
						//Fill user basic information in respective accordions
						populateUserBasicInformation(result);
						//To get the devicetoken and devide type information.
						getUserInstallation(appUserId);
						// Get current location and weather information of the users based on the users latest latitude and longitude
						getCurrentLocationWeatherInformation(lat, lng);
						// Get user interests
						getUserInterest(appUserId, appUserToken);	
						// Get answers of intent question by the user. 
						getUserAnswers(appUserId, appUserToken);
						// Get deals from sqoot near by users location
						getNearByDeals(key, lat, lng);
					}
				}
			});
		}
	}
});
}

//To get the user information and display it on basic info accordion container
function populateUserBasicInformation(result)
{
	var familyHTML = '';
	if (result.realm != "")
	{
		$("#callerimage").attr("src", result.realm);
		$("#callerbgimg").attr("src",result.realm);
	}
	if (result.details.profile_image != "")
	{
		$("#callerimage").attr("src", result.details.profile_image);
		$("#callerbgimg").attr("src",result.details.profile_image);
	}
	$("#inputPhone").val(result.details.phone);
	$("#inputAddress").val(result.details.street);
	$("#inputEmail").val(result.email);
	$("#inputCity").val(result.details.city);
	$("#inputState").val(result.details.state);
	$("#inputZip").val(result.details.zip);
	$("#inputOccupation").val(result.details.occupation);
	$("#callername").html(result.details.first_name + " " + result.details.last_name);
    $("#callernumber").html(result.details.phone);
	$("#callerlevel").html('Free');
	if (result.details.family)
	{
		familyHTML += "<b>Marital Status</b> : " + result.details.family.marital_status + "<br /><br />";
		if (result.details.family.children.length > 0)
		{
			familyHTML += "<b>Children</b> : Yes <br /><br />";
			
			for (var x = 0; x < result.details.family.children.length; x++)
			{
				if (result.details.family.children[x].name)
				{
					familyHTML += "<b>"+(x+1)+". Child Name</b> : " + result.details.family.children[x].name + "<br /><br />";
				}
				if (result.details.family.children[x].gender)
				{
					familyHTML += "<b>Child Gender</b> : " + result.details.family.children[x].gender + "<br /><br />";
				}
				if (result.details.family.children[x].birthday)
				{
					familyHTML += "<b>Child Birthday</b> : " + result.details.family.children[x].birthday + "<br /><br />";
				}
			}
		}
		else
		{
			familyHTML += "<b>Children</b> : No<br /><br />";
		}
	}
}

// To get tje weather information of the users current location.
function getCurrentLocationWeatherInformation(lat ,lng)
{
	var geocoder;
	geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(lat, lng);
	geocoder.geocode({'latLng': latlng}, function (results, status)
	{
		if (status == google.maps.GeocoderStatus.OK)
		{
			if (results[0])
			{
				var add = results[0].formatted_address;
				var value = add.split(",");
				count = value.length;
				country = value[count - 1];
				state = value[count - 2];
				city = value[count - 3];
				$(".city").html(city);
				$(".state").html(state);
				$.ajax({
					url: "https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='"+$(".city").html()+","+ $(".state").html()+"')&format=json",
					type: 'GET',
					data: {},
					success: function (resp) {
						if(resp.query.results)
						{																	
							var dt = resp.query.results.channel.item.condition.date;
							var day = dt.split(',')[0];
							var tempF = resp.query.results.channel.item.condition.temp;
							$("#temp").html(tempF);
							$(".day").html(getFullDay(day));
							var forecast = resp.query.results.channel.item.forecast;
							if(forecast.length > 0)
							{
								showForecast = true;
								$("#tblforecast").append("<tbody>");
								for(var i =0; i < forecast.length; i++)
								{
									$("#tblforecast").append("<tr><td>"+forecast[i].day+", "+forecast[i].date+"</td><td>"+forecast[i].high+"</td><td>"+forecast[i].low+"</td><td>"+forecast[i].text+"</td></tr>");
								}	
								$("#tblforecast").append("</tbody>");
							}												
						}
					}
				});
			}
		}
	});
}

//To get the users installation info
function getUserInstallation(userID)
{
	$.ajax({
		url: config.getapiurl()+"/installations?filter=%7B%22where%22%3A%7B%22userId%22%3A%22"+userID+"%22%7D%2C%22order%22%3A%22created%20DESC%22%2C%20%22limit%22%3A1%7D",
		type: 'GET',
		data: {},
		success: function (userinstalltion) {
			if(userinstalltion)
			{
				for(var i=0; i < userinstalltion.length; i++)
				{
					if(userinstalltion[i])
					{
						userDeviceToken = userinstalltion[i].deviceToken;
						userDeviceType = userinstalltion[i].deviceType;
					}
				}
			}
		}
	});
}

//To get the users interests
function getUserInterest(userID, acccessToken)
{
	$.ajax({
		url: config.getapiurl()+"/appUsers/"+userID+"/interests?access_token="+acccessToken,
		type: 'GET',
		data: {},
		success: function (userintrest) {
			if(userintrest)
			{
				for(var i=0; i < interests.length; i++)
				{
					if(interests[i])
					{
						var r = $.grep(userintrest, function(e){ if(e) return e.interestId == interests[i].id; });
						if(r.length == 1)
						{
							$("#intrest"+interests[i].id).prop("checked","true");
							$(".chk-container"+interests[i].id).show();
						}
						else
						{
							$(".chk-container"+interests[i].id).hide();
						}
					}
				}
			}
		}
	});
}

//Get deals near by users current location
function getNearByDeals(skey, ulat, ulng)
{
	$.ajax({
		url: "http://api.sqoot.com/v2/deals?api_key="+skey+"&order=distance&per_page=200&location=" + ulat + "," + ulng,
		type: 'GET',
		data: {},
		success: function (res) {
			var deals = res.deals;
			var evenHtml = '';
			var oddHtml = '';
			for (var i = 0; i < deals.length; i++)
			{
				var category = '';
				var title = '';
				var description = '';
				var address = '';
				var city = '';
				var state = '';
				var zip = '';
				var phone = '';
				var completeAddress = '';
				
				if (deals[i].deal.category_name)
				{
					category = deals[i].deal.category_name;
				}
				if (deals[i].deal.description)
				{
					description = deals[i].deal.description;
				}
				if (deals[i].deal.title)
				{
					title = deals[i].deal.title;
				}
				if (deals[i].deal.merchant.address)
				{
					address = deals[i].deal.merchant.address;
					completeAddress += address + ", ";
				}
				if (deals[i].deal.merchant.locality)
				{
					city = deals[i].deal.merchant.locality;
					completeAddress += city + ", ";
				}
				if (deals[i].deal.merchant.region)
				{
					state = deals[i].deal.merchant.region;
					completeAddress += state + " ";
				}
				if (deals[i].deal.merchant.postal_code)
				{
					zip = deals[i].deal.merchant.postal_code;
					completeAddress += zip + ", ";
				}
				if (deals[i].deal.merchant.phone_number)
				{
					phone = deals[i].deal.merchant.phone_number;
					completeAddress += phone;
				}
				if (i == 0 || i == 2)
				{
					evenHtml += '<div class="rating_box1">';
					evenHtml += '<div class="number number1">' + (i + 1) + '</div>';
					evenHtml += '<div class="sushi pull-left">';
					evenHtml += '<h1>' + title + '</h1>'
					evenHtml += '<p class="rate"><span class="color_orange">4.3 <img src="images/ratingstar2.png" alt=""></span> 39 Reviews - $$ ' + (category != '' ? '- ' + category : category) + '<br /> <a href="javascript:void(0)" onclick="sendNotification('+deals[i].deal.id+')" class="sendbutton">Send</a></p>';
					evenHtml += '<p>' + description + '</p>';
					evenHtml += '<span>' + completeAddress + '</span>';
					evenHtml += '</div>';
					evenHtml += '<div class="sushi_img pull-right">';
					evenHtml += '<img src="' + deals[i].deal.image_url + '" />';
					evenHtml += '</div>';
					evenHtml += '<div class="clear"></div>';
					evenHtml += '</div>';
				}
				else if (i == 1 || i == 3)
				{
					oddHtml += '<div class="rating_box1">';
					oddHtml += '<div class="number number1">' + (i + 1) + '</div>';
					oddHtml += '<div class="sushi pull-left">';
					oddHtml += '<h1>' + title + '</h1>';
					oddHtml += '<p class="rate"><span class="color_orange">4.3 <img src="images/ratingstar2.png" alt=""></span> 39 Reviews - $$ ' + (category != '' ? '- ' + category : category) + ' <br /> <a href="javascript:void(0)" onclick="sendNotification('+deals[i].deal.id+')" class="sendbutton">Send</a></p>';
					oddHtml += '<p>' + description + '</p>';
					oddHtml += '<span>' + completeAddress + '</span>';
					;
					oddHtml += '</div>';
					oddHtml += '<div class="sushi_img pull-right">';
					oddHtml += '<img src="' + deals[i].deal.image_url + '" />';
					oddHtml += '</div>';
					oddHtml += '<div class="clear"></div>';
					oddHtml += '</div>';
				}
				
				var ltLn = new google.maps.LatLng(deals[i].deal.merchant.latitude, deals[i].deal.merchant.longitude);
				var image = {
					url: 'images/marker_green.png',
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};

				// Create a marker for each place.
				var marker = new google.maps.Marker({
					map: map,
					icon: image,
					title: deals[i].deal.title,
					position: ltLn,
				});
				marker.category = deals[i].deal.category_slug;
				gMarkers.push(marker);
				var dealid = deals[i].deal.id;
				var infowindow = new google.maps.InfoWindow();
				var content = '<table border="1" cellpadding="0" cellspacing="0" style="width:98%;">';
				content += '<tr>';
				content += '<td style="vertical-align:top; padding:5px;">';
				content += '<p class="deal-title">'+ deals[i].deal.title + '</p><p class="deal-category">' + deals[i].deal.category_name + '</p><p class="deal-subtitle">' + deals[i].deal.short_title + '</p><p class="deal-address">' + completeAddress+"</p>";
				content += '</td>';
				content += '<td style="vertical-align:top; padding:5px;">';
				content += '<img src="'+deals[i].deal.image_url+'" class="deal-photo">';
				content += '</td>';
				content += '</tr>'
				content += '<tr>';
				content += '<td colspan="2" style="padding-bottom: 5px;">';
				content += '<a href="javascript:void(0)" onclick="sendNotification('+dealid+')" class="sendbutton">Send</a>';
				content += '</td>';
				content += '</tr>';
				content += '</table>'; 
				google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
					return function() {
						if(currentinfo) { currentinfo.close();} 
						infowindow.setContent(content);
						google.maps.event.addListener(infowindow, 'domready', function() {
						    $('a#aUrl').zclip({
								path:'js/ZeroClipboard.swf',
								copy:$('#hfUrl').val(),
								afterCopy:function(){
									$('#txtMessage').val($('#hfUrl').val());
								}
							});
						});
						infowindow.open(map,marker);
						currentinfo = infowindow; 
						e.preventDefault();
					};
				})(marker,content,infowindow));
			}
			$(".dealodd").html(evenHtml);
			$(".dealeven").html(oddHtml);
		}
	});	
}

// To get all the intent questions
function populateQuestions()
{
	$.ajax({
		url: config.getapiurl()+"/questions",
		type: 'GET',
		data: {},
		success: function (resquestion) {
			questions = resquestion;			
			for (var i = 0; i < questions.length; i++)
			{
				setInnerContent(questions[i].type, questions[i].options, questions[i].text, questions[i].id, 'collapse'+questions[i].section);
			} 
		}
	});
}

//Set the questions of the intent in the respective intent accordion container
function setInnerContent(type, options, label, questionid, containername)
{
	var html = '';
	if(type == "text")
	{
		html += '<label for="">'+label+':</label>';
		html += '<input type="text" id="'+questionid+'" name="'+questionid+'" value="" placeholder="" />';
	}
	else if(type == "select")
	{
		html += '<label for="">'+label+':</label>';
		html += '<select id="'+questionid+'" name="'+questionid+'">';
		html += '<option value="0">Select</option>';
		if(options)
		{
			for(var i =0; i < options.length; i++)
			{
				html += '<option value="'+options[i]+'">'+options[i]+'</option>';;
			}
		}
		html += '</select>';
	}
	$("#"+containername+" .panel-body .col-xs-12").append(html);
}

//Get the user answers for the intent questions
function getUserAnswers(userID, acccessToken)
{
	$.ajax({
		url: config.getapiurl()+"/appUsers/"+userID+"/userAnswers?access_token="+acccessToken,
		type: 'GET',
		data: {},
		success: function (resAnswers) {
			var answers = resAnswers;
			for (var i = 0; i < answers.length; i++)
			{
				setUserAnswers(answers[i].questionId, answers[i].response);
			} 

			for(var i=0; i < questions.length; i++)
			{
				if(questions[i])
				{
					var r = $.grep(answers, function(e){ if(e) return e.questionId == questions[i].id; });
					if(r.length == 0)
					{
						$(".heading-"+questions[i].section).css("background-color", "#ff0000");
					}
				}
			}
		}
	});
}

//Set user answers
function setUserAnswers(questionid, answers)
{
	$("#"+questionid).val(answers);
}

// To populate the interests lists
function populateIntrests()
{
	$(".interest-right").html('');
	$(".interest-left").html('');
	$.ajax({
		url: config.getapiurl()+"/interests",
		type: 'GET',
		data: {},
		success: function (interes) {
			interests = interes;
			for (var i = 1; i <= interests.length; i++)
			{
				if(i%2 == 0)
				{
					$(".interest-right").append('<div class="checkbox check-primary chk-container'+interests[i-1].id+'"><input id="intrest'+interests[i-1].id+'" type="checkbox" value="'+interests[i-1].name+'" onclick="applyCategoryfilter(this);"><label for="intrest'+interests[i-1].id+'">'+interests[i-1].name+'</label></div>');
				}
				else
				{
					$(".interest-left").append('<div class="checkbox check-primary chk-container'+interests[i-1].id+'"><input id="intrest'+interests[i-1].id+'" type="checkbox" value="'+interests[i-1].name+'" onclick="applyCategoryfilter(this);"><label for="intrest'+interests[i-1].id+'" >'+interests[i-1].name+'</label></div>');
				}
			} 
		}
	});
}

// To filter the interests
function filterIntrest(term)
{
    term = term.toLowerCase();
	$(".interestwrap-2 input[type=checkbox]").each(function(){
		if($(this).next('label').text().toLowerCase().indexOf(term) != -1)
		{
			$(this).parent('.check-primary').show();
		}
		else
		{
			$(this).parent('.check-primary').hide();
		}
	});
	e.preventDefault();
}

// Get categories from Sqoot and populate the category list
function populateCategories(key,agentname)
{
	agentname = agentname;
	$.ajax({
		url: "http://api.sqoot.com/v2/categories?api_key="+key,
		type: 'GET',
		data: {},
		success: function (res) {
			categories = res.categories;
			for (var i = 1; i <= categories.length; i++)
			{
				slugs.push(categories[i-1].category.slug);
				if(i%2 == 0)
				{
					$(".category-right").append('<div class="checkbox check-primary"><input id="category'+(i-1)+'" type="checkbox" value="'+categories[i-1].category.slug+'" onclick="applyCategoryfilter(this);"><label for="category'+(i-1)+'">'+categories[i-1].category.name+'</label></div>');
				}
				else
				{
					$(".category-left").append('<div class="checkbox check-primary"><input id="category'+(i-1)+'" type="checkbox" value="'+categories[i-1].category.slug+'" onclick="applyCategoryfilter(this);"><label for="category'+(i-1)+'" >'+categories[i-1].category.name+'</label></div>');
				}
			}
		}
	});
}
// Filter deals as per the selected categories			
function applyCategoryfilter(obj)
{
	var cslugs = $("#txtSlugs").val();
	var slug = $(obj).val();
	if($(obj).is(":checked"))
	{
		cslugs+=slug+",";						
	}
	else
	{
		cslugs = cslugs.replace(slug+',','');
	}
	$("#txtSlugs").val(cslugs);					
	if(cslugs != "")
	{
		for (var i=0; i< gMarkers.length; i++) 
		{
			if(gMarkers[i].category != null)
			{
				var cvalue = gMarkers[i].category.toString();			
				if (cvalue.indexOf(',') == -1 && cslugs.indexOf(cvalue) != -1) {
					gMarkers[i].setVisible(true);
				}
				else if (cvalue.indexOf(',') > 0) 
				{ 
					var flag = false;
					var c = cvalue.split(',');
					if(c.length > 0)
					{
						for(var j = 0; j < c.length; j++)
						{
							if(cslugs.indexOf(c[j]) != -1)
							{
								flag = true;
							}
						}
					}				
					if(flag)
					{
						gMarkers[i].setVisible(true);
					}
					else{
						gMarkers[i].setVisible(false);
					}
				}
				else{
					gMarkers[i].setVisible(false);
				}
			}
			else{
				gMarkers[i].setVisible(false);
			}
		}
	}
	else{
		for (var i=0; i< gMarkers.length; i++) {
			gMarkers[i].setVisible(true);
		}
	}
}

function getFullDay(val)
{
	var day = '';
	if(val == "Mon")
	{
		day = "Monday";
	}
	else if(val == "Tue")
	{
		day = "Tuesday";
	}
	else if(val == "Wed")
	{
		day = "Wednesday";
	}
	else if(val == "Thu")
	{
		day = "Thursday";
	}
	else if(val == "Fri")
	{
		day = "Friday";
	}
	else if(val == "Sat")
	{
		day = "Saturday";
	}
	else if(val == "Sun")
	{
		day = "Sunday";
	}
	return day;
}

function filterCategories(term)
{
    term = term.toLowerCase();
	$(".checkboxwrap-2 input[type=checkbox]").each(function(){
		if($(this).next('label').text().toLowerCase().indexOf(term) != -1)
		{
			$(this).parent('.check-primary').show();
		}
		else
		{
			$(this).parent('.check-primary').hide();
		}

	});
	e.preventDefault();
}

// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
function initialize(userLat,userLng) {
	var latLng = new google.maps.LatLng(userLat, userLng);
	var markers = [];
	map = new google.maps.Map(document.getElementById('map-canvas'), {
		center: latLng,
		zoom: 12,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		panControl: true,
		panControlOptions: {
			position: google.maps.ControlPosition.RIGHT_CENTER
		},
		zoomControl: true,
	    zoomControlOptions: {
	        style: google.maps.ZoomControlStyle.LARGE,
	        position: google.maps.ControlPosition.RIGHT_CENTER
	    },
	});

	var defaultBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(userLat, userLng),
		new google.maps.LatLng(userLat, userLng));
	// Create a marker for each place.
	var mark = new google.maps.Marker({
	   position: latLng,
	   map: map
	 });
	// Create the search box and link it to the UI element.
	var input = (document.getElementById('pac-input'));
	map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);
	var infowindow = new google.maps.InfoWindow();
	var searchBox = new google.maps.places.SearchBox((input));
	// Listen for the event fired when the user selects an item from the
	// pick list. Retrieve the matching places for that item.
	google.maps.event.addListener(searchBox, 'places_changed', function () {
		var places = searchBox.getPlaces();
		
		if (places.length == 0) {
			return;
		}
		for (var i = 0, marker; marker = markers[i]; i++) {
			marker.setMap(null);
		}

		// For each place, get the icon, place name, and location.
		markers = [];
		var bounds = new google.maps.LatLngBounds();
		for (var i = 0, place; place = places[i]; i++) {
			var image = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			var marker = new google.maps.Marker({
				map: map,
				icon: image,
				title: place.name,
				position: place.geometry.location,
			});
			marker.category =  place.types;
			gMarkers.push(marker);
			var request = {
				reference: place.reference
			};
			var content = '<h5>' + place.name + '</h5><p>' + place.formatted_address;
			if (!!place.formatted_phone_number)
				content += '<br>' + place.formatted_phone_number;
			if (!!place.website)
				content += '<br><a target="_blank" href="' + place.website + '">' + place.website + '</a>';
			content += '<br>' + place.types + '</p>';
			//content += '<br>' + place + '</p>';
			google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
				return function() {
					if(currentinfo) { currentinfo.close();} 
					infowindow.setContent(content);
					infowindow.open(map,marker);
					currentinfo = infowindow;
				};
			})(marker,content,infowindow)); 
			markers.push(marker);
			bounds.extend(place.geometry.location);
		}
		map.fitBounds(bounds);
	});
	// Bias the SearchBox results towards places that are within the bounds of the
	// current map's viewport.
	google.maps.event.addListener(map, 'bounds_changed', function () {
		var bounds = map.getBounds();
		searchBox.setBounds(bounds);
	});
}
//google.maps.event.addDomListener(window, 'load', initialize);            
function starttimer() {
	$(".btn-hangup").show();
	$(".btn-forward").show();
	setInterval(function(){
	    ++seconds;
	    $('#timer').html(pad(parseInt(seconds / 60)) + ":" + pad(seconds % 60));
	},1000);
}
function pad(val) {
    var valString = val + "";
    return (valString.length < 2) ? "0" + valString : valString;
}

function showForeCast()
{
	if(showForecast)
	{
		$('#forecastModal').modal('show');
	}  
}

function getCallerInQueue() {
	$.ajax({
		url: config.getsiteurl()+"/getqueues.php",
		type: 'GET',
		data: {},
		success: function (resQueue) {
			$('#collapseSeven .panel-body').html(resQueue);
		}
	});
}

function callQueue(obj)
{
    var sid = $(obj).attr('id').toString().split(':')[0];
    var csid = $(obj).attr('id').toString().split(':')[1];

    $.ajax({
		url: config.getsiteurl()+"/dequeue.php",
		type: 'POST',
		data: {sid : sid, csid : csid},
		success: function (resQueue) {
			console.log("dequeueResponse",resQueue);
		}
	}); 
}
function getCallerInProgress() {
    $.ajax({
		url: config.getsiteurl()+"/getcallinprogress.php",
		type: 'GET',
		data: {},
		success: function (resQueue) {
			$('#collapseEight .panel-body').html(resQueue);
		}
	}); 
}

$(document).ready(function () {
	getResolutions();
	$("#searchInterest").keyup(function(e){
	    var term = $(this).val().toLowerCase();	    
	    if(e.which != 13)
	    {
		    $(".interestwrap-2 input[type=checkbox]").each(function(){
		        if($(this).next('label').text().toLowerCase().indexOf(term) != -1)
		        {
		            $(this).parent('.check-primary').show();
		        }
		        else
		        {
		            $(this).parent('.check-primary').hide();
		        }
		    });
		    e.preventDefault();
		}
		else
		{
			if(term != "")
    		{
				term = term.charAt(0).toUpperCase() + term.slice(1);
				var r = $.grep(interests, function(e){ if(e) return e.name.indexOf(term) != -1; });
				if(r.length == 0)
				{					
					$.ajax({
						url: config.getapiurl()+"/interests",
						type: 'POST',
						data: {name : term, approved : false},
						success: function (resp) {
							var interestID = resp.id;
							$.ajax({
								url: config.getapiurl()+"/appUsers/"+appUserId+"/interests?access_token="+appUserToken,
								type: 'POST',
								data: {interestId : interestID, appUserId : appUserId, name : term},
								success: function (respui) {
									populateIntrests();
									getUserInterest(appUserId, appUserToken);
									$("#searchInterest").val('');
								}
							});									
						}
					});
				}				
			} 
		}
	});
	$("#searchCategory").keyup(function(e)
	{
	    var term = $(this).val().toLowerCase();
	    $(".checkboxwrap-2 input[type=checkbox]").each(function(){
	        if($(this).next('label').text().toLowerCase().indexOf(term) != -1)
	        {
	            $(this).parent('.check-primary').show();
	        }
	        else
	        {
	            $(this).parent('.check-primary').hide();
	        }

	    });
	    e.preventDefault();
	});
});
function getNotes(uid) {

	$.ajax({
		url: config.getapiurl()+"/logs?filter=%7B%22where%22%3A%7B%22source%22%3A%22"+agentname+"%22%2C%22appUserId%22%3A%22"+uid+"%22%7D%7D",
		type: 'GET',
		success: function (data) {
			var notes = data;
			var stringNotes="<ul>";
			for (var i = 0; i < notes.length; i++)
			{
				var c = notes[i].comment.toString().split(':');
				var appuid = notes[i].appUserId;
				if(appuid && appuid.length>12){
					appuid = appuid.substr(notes[i].appUserId.toString(),12);
				}
				if(c.length>1){
					stringNotes += "<li><span title='"+notes[i].appUserId.toString()+"' class='one'>"+appuid+"</span><span class='two' style='width:400px !important;'>"+c[0]+"</span><span class='two' style='width:200px !important;'>"+c[1]+"</span><span class=''>"+new Date(parseInt(notes[i].timestamp)).toLocaleString()+"</span></a></li>";
				}else{
					stringNotes += "<li><span class='one' title='"+notes[i].appUserId.toString()+"'>"+appuid+"</span><span class='two' style='width:400px !important;'>"+c[0]+"</span><span class='two' style='width:200px !important;'>&nbsp;-&nbsp;</span><span class=''>"+new Date(parseInt(notes[i].timestamp)).toLocaleString()+"</span></a></li>";
				}
				
			}
			stringNotes += "</ul>";
			$("#notesinner").html(stringNotes);
		}
	});
}

function getResolutions() {

	$.ajax({
		url: config.getsiteurl()+"/json/resolutions.json",
		type: 'GET',
		data: {},
		success: function (data) {
			var resolutions = data.resolutions;
			var optionsAsString = "";
			for (var i = 0; i < resolutions.length; i++)
			{
				optionsAsString += "<option value='" + resolutions[i].resolution + "'>" + resolutions[i].resolution + "</option>";
			}			 
			$( 'select[name="agentnotesresolution"]' ).append( optionsAsString );
		}
	});
}
function savenotes(source,comment,timestamp,appUserId,comment2) {
	$.ajax({
        url: config.getapiurl()+"/logs",
        type: 'POST',
        data: {
                "source":source,
                "comment":comment,
                "timestamp":timestamp,
                "appUserId":appUserId
               },
        success: function (res) {
            $.ajax({
					url: config.getapiurl()+"/logs",
					type: 'POST',
					data: {
						"source":source,
						"comment":comment2,
						"timestamp":timestamp,
						"appUserId":appUserId
					},
					success: function (res) {
						$("#btnNotesSave").hide();
						$("#btnCloseNotes").show();
						$("#spanNotesSaved").attr('style','color:green;font-weight:bold;');
						$("#spanNotesSaved").html("Notes has been saved successfully. <br/><br/>");
					},
					error:function(err){
						$("#btnNotesSave").show();
						$("#btnCloseNotes").hide();
						$("#spanNotesSaved").attr('style','color:red;font-weight:bold;');
						$("#spanNotesSaved").html("Oops! something went wrong, please re-try.");
					}
			}); 
    	},
        error:function(err){
        	$("#btnNotesSave").show();
        	$("#btnCloseNotes").hide();
			$("#spanNotesSaved").attr('style','color:red;font-weight:bold;');
            $("#spanNotesSaved").html("Oops! something went wrong, please re-try.");
        }
  }); 
}
function saveintentresolutionnotes(source,comment,timestamp,appUserId,comment2) {
	$.ajax({
		url: config.getapiurl()+"/logs",
		type: 'POST',
		data: {
			"source":agentname,
			"comment":comment2,
			"timestamp":timestamp,
			"appUserId":appUserId
		},
		success: function (res) {
			//window.location.reload();  
			$('#notesModal').modal('hide');
		},
		error:function(err){
			//window.location.reload(); 
			$('#notesModal').modal('hide'); 
		}
	}); 	            
}

function clearerror(){
	$("#spanNotesSaved").html("");
}
function closenotes(){
	//window.location.reload();
	$('#notesModal').modal('hide');
}

function sendNotification(dealid)
{
	$.ajax({
		url: "http://api.sqoot.com/v2/deals/"+dealid+"?api_key="+k,
		type: 'GET',
		data: {},
		success: function (res) {
			var deal = res.deal;
			var postData = new Object();
			var dealData = new Object();
			var merchantData = new Object();
			postData.deviceType = userDeviceType;
			postData.deviceToken = userDeviceToken;
			postData.alert = "any";
			postData.badge = "1";
			postData.sound = "ping.aiff";
			postData.collapseKey = "";
			postData.delayWhileIdle = "false";
			postData.created = new Date().getTime();
			postData.expirationInterval = "0";
			postData.status = "";
			postData.appUserId = appUserId;
			dealData.category_name = deal.category_name;
			dealData.description = deal.description;
			dealData.discount_amount = deal.discount_amount;
			dealData.discount_percentage = deal.discount_percentage;
			dealData.id = deal.category_name;
			dealData.image_url = deal.image_url;
			dealData.price = deal.price;
			dealData.provider_name = deal.provider_name;
			dealData.short_title = deal.short_title;
			dealData.title = deal.title;
			dealData.updated_at = deal.updated_at;
			dealData.url = deal.url;
			dealData.value = deal.value;

			postData.deal = dealData;

			merchantData.address = deal.merchant.address;
			merchantData.country = deal.merchant.country;
			merchantData.country_code = deal.merchant.country_code;
			merchantData.id = deal.merchant.id;
			merchantData.latitude = deal.merchant.latitude;
			merchantData.locality = deal.merchant.locality;
			merchantData.longitude = deal.merchant.longitude;
			merchantData.name = deal.merchant.name;
			merchantData.postal_code = deal.merchant.postal_code;
			merchantData.region = deal.merchant.region;

			postData.merchant = merchantData; 

            $.ajax({
				url: config.getapiurl()+"/notifications",
				type: 'POST',
				data: postData,
				success: function (resp) {
					if(resp)
                    {
                        alert('Notification sent successfully');
                    }
				},
				error:function(err){
					alert('Error in processing your request.');
				}
			}); 
		}
	});
}

function resetdashboardcallvariables(){
	seconds = 0;
	gMarkers = [];
	categories = [];
	interests = [];
	questions = [];
	slugs = [];
	currentinfo = null;
	showForecast = false;
	appUserId = '';
	appUserToken = ''; 
	userDeviceToken = ''
	userDeviceType = '';
	$("#inputPhone").val("");
	$("#inputAddress").val("");
	$("#inputEmail").val("");
	$("#inputCity").val("");
	$("#inputState").val("");
	$("#inputZip").val("");
	$("#inputOccupation").val("");
	$('#timer').html("00:00");
	$("#callername").html("Ed Leon");
    	$("#callernumber").html("(502) 553-97 05");
	$("#callerlevel").html('MEMBERSHIP LEVEL');
	$("#temp").html("89");
	$(".day").html("SUNDAY");
	$("#tblforecast tbody").html("<thead><tr><th>Date</th><th>High</th><th>Low</th><th>Notes</th></tr></thead>");
	$("#collapsefamily .panel-body .col-xs-12").html("");
	$("#collapsehealth .panel-body .col-xs-12").html("");
	$("#collapsepersonalinjury .panel-body .col-xs-12").html("");
	$("#collapsebankruptcy .panel-body .col-xs-12").html("");
	$("#collapsedui .panel-body .col-xs-12").html("");
	$("#collapsetax .panel-body .col-xs-12").html("");
	$("#collapselegaldocuments .panel-body .col-xs-12").html(""); 
	$("#collapsesupplemental .panel-body .col-xs-12").html("");
	$("#collapselife .panel-body .col-xs-12").html("");
	$("#collapsedisability .panel-body .col-xs-12").html("");
	$("#collapselongtermcare .panel-body .col-xs-12").html("");
	$("#collapseentertainment .panel-body .col-xs-12").html("");
	$("#collapsedining .panel-body .col-xs-12").html("");
	$("#collapsetravel .panel-body .col-xs-12").html("");
	$("#collapsehomeauto .panel-body .col-xs-12").html("");
	$("#collapsesirvipays .panel-body .col-xs-12").html("");
	$(".dealodd").html("");
	$(".dealeven").html("");
	$(".interest-left").html("");
       $(".interest-right").html("");
	$("#callerimage").attr("src", "images/noimage.png");
	$( ".panel-heading" ).each(function( index ) {
	   $( this ).css("background-color", "#292F6C");
	});
	
}