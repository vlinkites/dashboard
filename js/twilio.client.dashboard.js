var config = new settings();
var curcon;
Twilio.Device.setup(tc);
var CallSid = '';
var from = '';
var currentuid='' ;
var intent = '';
var currentcallstatus='';
var ismicrophoneattached=false;
var ishangup=false;
Twilio.Device.ready(function (device) {    
    console.log('Ready');
    $('#__connectionFlash__')
    .wrap('<div class="fl-wrapper">') // Wrap the flash object in a div.
    .parent().css({'overflow':'hidden'}) // Set the wrapper to overflow hidden.
    .children().css({'margin-left':-99999});  // Set flash object to be out of box.
    connectFirstQueuedCall(false); // Trigger timer & Just call to show if any callers in queue
});

Twilio.Device.error(function (error) {
    console.log("Error: " + error.message);
    if(currentcallstatus=='queued' || currentcallstatus=='in-progress'){
        $('#notesModal').modal('show');
    }else{
       window.location.reload();    
       //resetcallvariables();
    }
});
Twilio.Device.connect(function (conn) {
    starttimer();
    if(from == '' && CallSid == '')
    {
       CallSid = conn.parameters.CallSid;
       from = conn.parameters.From;
    }
    console.log("Successfully established call ");
});

Twilio.Device.disconnect(function (conn) {
  console.log("Call ended");    
  if(currentcallstatus=='queued' || currentcallstatus=='in-progress'){
    $('#notesModal').modal('show');
  }else{
   //window.location.reload();	
   resetcallvariables();
  }
});

Twilio.Device.cancel(function(conn) {
    console.log(conn.parameters.From); // who canceled the call
    $('#myModal').modal('hide');
    if(currentcallstatus=='queued' || currentcallstatus=='in-progress'){
        $('#notesModal').modal('show');
    }else{
        //window.location.reload();
	 remainingtime=10;
	 runner();	
        resetcallvariables();
    } 
});

Twilio.Device.incoming(function (conn) {
    curcon = conn;
    console.log(conn.parameters);
    // accept the incoming connection and start two-way audio
    var cid = conn.parameters.CallSid;
    var frm = conn.parameters.From;
    intent = 'family';
    if(conn.parameters.intent) 
    {
        intent = conn.parameters.intent.toLowerCase();
    }
    $(".intent-holder").html(intent.toUpperCase());
    $("#collapse"+intent).addClass('in');
    if(intent == 'law')
    {
        frm = frm.toString().replace("+", "");
        $.get(config.getsiteurl()+"/callforward.php?csid="+cid+"&from="+frm+"&to=18003219929").success(function(){
            console.log('Call transferred');
        });
    }
    else
    {
        var lat = 0;
        var lng = 0;
        var uid = conn.parameters.From;
        uid = uid.replace("client:", "");
        var at = '';      
        currentuid = uid;
    }    
});

function call() {
    curcon.accept();
    $('#myModal').modal('hide');
	$.ajax({
        url: config.getsiteurl()+"/getcalldetails.php?csid="+curcon.parameters.CallSid,
        success: function (res) {
        currentcallstatus=res.toString();
    },
    error:function(){
        currentcallstatus="";
    }
    }); 
}

var checkqueue=true;
var remainingtime=10;
function runner() {
    $("#spanstartcallheader").html("Waiting to connect call...");
    $("#spandashboardconnectqueuetimer").html("<span style='font-size:20px;font-weight:bold;'>Connecting call in "+remainingtime+" seconds...</span>");
    if(remainingtime==0){
        $("#spandashboardconnectqueuetimer").html("<span style='font-size:20px;font-weight:bold;'>Checking the queue...</span>");
        connectFirstQueuedCall(true);
        return;
    }
    remainingtime = (remainingtime-1);
if(remainingtime <0){
remainingtime=0;
}
    setTimeout(function() {
        runner();
    }, 1000);
}

function checkmicrophone(){
    navigator.getUserMedia = ( navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
    if (navigator.getUserMedia) {
        navigator.getUserMedia (
        // constraints
        {
            video: false,
            audio: true
        },
        // successCallback
        function(localMediaStream) {
            ismicrophoneattached = true;
            $("#divmaincounter").show();
            $("#divnomicrophone").hide();
	     remainingtime=10;
            runner();
            // Do something with the video here, e.g. video.play()
        },
        // errorCallback
        function(err) {
            ismicrophoneattached = false;
            $("#divmaincounter").hide();
            $("#divnomicrophone").show();
            $("#spanstartcallheader").html("Oops! Please check your pc is connected with the microphone.");
        });
    } else {
        ismicrophoneattached = false;
        $("#divmaincounter").hide();
        $("#divnomicrophone").show();
        $("#spanstartcallheader").html("Oops! Please check your pc is connected with the microphone.");
    }
}

function connectFirstQueuedCall(iscount) {
    $.ajax({
        url: config.getsiteurl()+"/getqueues.php?getfirstqueue=true",
        type: 'GET',
        data: {},
        success: function (resQueue) {
            var callarray = resQueue.split('@');
            if(callarray && callarray.length===4 && callarray[0] >0){
            $("#spanqueuedmembercount").html(callarray[0].toString());
            $("#spantimer").html("Average wait time - "+callarray[1].toString());
            if(iscount){
                var obj = {CallSid:callarray[2],from:callarray[3]};
                $("#divmainstart").hide();
                $("#divmaindashboard").show();
                queuecall(null,obj);
            }else{
                checkmicrophone();
            }
        }else{
            $("#spanqueuedmembercount").html("0");
            $("#spantimer").html("Average wait time - N/A");
            if(iscount){
                $("#spanstartcallheader").html("No callers in queue.");
                $("#spandashboardconnectqueuetimer").html("<span style='font-size:20px;font-weight:bold;'>No callers in queue.</span>");
            }
            remainingtime=10;
            setTimeout(function() {
			 if(ismicrophoneattached==false){
			        checkmicrophone();
		        }else{
		              runner();
		        }
            }, 700);
        }
    },
    error:function(){
        $("#spanqueuedmembercount").html("0");
        $("#spantimer").html("Average wait time - N/A");
        $("#spanstartcallheader").html("Oops! Network issue... Re-trying...");
        remainingtime=10;
        setTimeout(function() {
            runner();
        }, 1500);
    }}); 
}

function queuecall(obj,iterateobj) {
    if(obj){
        from = $(obj).attr('id').toString().split(':')[2];
        CallSid = $(obj).attr('id').toString().split(':')[1];
    } else if (iterateobj){
        from = iterateobj.from;
        CallSid = iterateobj.CallSid;
    }
    var lat = 0;
    var lng = 0;
    var uid = from;
    uid = uid.replace("client:", "");
    var at = '';
    intent = 'family';
    $(".intent-holder").html(intent.toUpperCase());
    $("#collapse"+intent).addClass('in');
    popData(uid, at, lat, lng,k);
    populateIntrests();
    populateQuestions(); 
    var params = {"q": "1"}; 
    setTimeout(function() {
        Twilio.Device.connect(params); 
     }, 1500);      
    currentuid = uid;
    currentcallstatus="in-progress";//Need to call ajax for checking sid value
}

function hangup() {
    Twilio.Device.disconnectAll();
    from = '';
    CallSid = '';
    ishangup = true;
    //$('#myModal').modal('hide');
}

function transfercall(e,to){
    e.preventDefault();
    from = from.toString().replace("+", "");
    $.get(config.getsiteurl()+"/callforward.php?csid="+CallSid+"&from="+from+"&to="+to).success(function(){
        $("#log").text('Call transferred to ' + to);
    });
}

$(document).ready(function () {
    var notespopuptimer=120;
    var stoptimer=false;
    function setnotespopuptimer() {
        if(notespopuptimer==0 || stoptimer == true){
            saveAgentNotes("","",true);
	       return;
        }
        notespopuptimer= (notespopuptimer-1);
        setTimeout(function() {
            setnotespopuptimer();
        }, 1000);
    }	
    $('#notesModal').on('hidden.bs.modal', function () {
        //window.location.reload();
        resetcallvariables("true");
    });
    $('#notesModal').on('show.bs.modal', function (e) {
	   notespopuptimer=120;
	   setnotespopuptimer();
    });
});

function saveAgentNotes(agentid,agentname,onlyintentresolution){
    var timestamp = new Date().getTime();
    if($("#agentnotes").val().trim()=="" && !onlyintentresolution){
        $("#spanNotesSaved").attr('style','color:red;font-weight:bold;');
        $("#spanNotesSaved").html("Please enter notes.");
        return;
    }
    $("#btnNotesSave").hide();
    if(onlyintentresolution){
        saveintentresolutionnotes(agentname,$("#agentnotes").val(),timestamp,currentuid,"INTENT - "+$("#agentnotesintent").val()+" : RESOLUTION - "+$("#agentnotesresolution").val());
    }else{
        stoptimer = true;
        savenotes(agentname,$("#agentnotes").val(),timestamp,currentuid,"INTENT - "+$("#agentnotesintent").val()+" : RESOLUTION - "+$("#agentnotesresolution").val());    
    }
    setTimeout(function()
    {
        resetcallvariables();
    },1500);
}

function resetcallvariables(notrunner){
    if(!ishangup){
        Twilio.Device.disconnectAll();
    }
    checkqueue=true;
    stoptimer=false;
    resetdashboardcallvariables();
    CallSid = '';
    from = '';
    currentuid='' ;
    intent = '';
    currentcallstatus='';
    notespopuptimer=120;
    ishangup = false;
    setTimeout(function()
    {
        $("#divmainstart").show();
        $("#divmaindashboard").hide();
        $('#notesModal').modal('hide');
	 if(notrunner=="true"){
		remainingtime=10;
   		runner();
	 }
    },700);
    $("#spanqueuedmembercount").html("0");
    $("#spantimer").html("Average wait time - N/A");
}