<div class="wrapper">
    <div class="row">
        <div class="container">
		    <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-9 nopaddingrt header-left">
                        <div class="brow">
                            <p class="waiting"><span id="spanstartcallheader">Waiting to connect call...</span></p>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <div class="customer">
                            <div class="line"></div>
                            <div class="pull-left">
                                <h1><?php echo $username; ?></h1>
                                <p>CUSTOMER SERVICE REP</p>
                                <div class="checkbox check-primary">
                                    <input id="LOG" type="checkbox" value="1">
                                    <label for="LOG"><a href="logout.php" style="color:#fff;">LOG ME OUT</a></label>
                                </div>
                            </div>
                            <div class="profile pull-right">
                                <img src="<?php echo $_SESSION["imageurl"]; ?>" alt="" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
		<!--header row closed-->			
            <div class="row" id="divmaincounter" style="display:none;">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="top"></div>
                    <div class="hello">
                        <div class="logo">
                            <img src="images/logo.png" /></div>
                        <h1>HELLO <?php echo $username; ?></h1>
                        <div class="login_main">
                            <div class="pull-left-1">
								     <div class="call-timings">
								        <p>There are <span id="spanqueuedmembercount">0</span> callers in Queue</p>
									    <p><span id="spandashboardconnectqueuetimer" class="conct-to-cl"  style="font-size:20px;font-weight:bold;">Getting ready to call</span></p>
										<p><i class="fa fa-clock-o"></i><span id="spantimer">Average wait time - N/A</span></p>
									</div>
                            </div>
                           
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="bottom"></div>
                </div>
            </div>
            <div class="row" id="divnomicrophone" >
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="top"></div>
                    <div class="hello">
                        <div class="logo">
                            <img src="images/logo.png" /></div>
                        <h1>HELLO <?php echo $username; ?></h1>
                        <div class="login_main">
                            <div class="pull-left-1">
                                     <div class="call-timings">
                                        <p><span id="spandashboardconnectqueuetimer" class="conct-to-cl"  style="font-size:20px;font-weight:bold;">Please allow microphone before connecting a caller. Or Please attach before connecting a caller.</span></p>
                                    </div>
                            </div>
                           
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="bottom"></div>
                </div>
            </div>
        </div>
    </div>
</div>